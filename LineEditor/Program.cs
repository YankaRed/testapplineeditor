﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace LineEditor
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length != 0)
            {
                var textFileName = args[0];
                if (File.Exists(textFileName))
                {
                    var text = File.ReadAllText(path: textFileName);
                    MessageBox.Show(text);
                    EditFile(textFileName);
                }
                else
                {
                    Console.WriteLine("\nFile does not exist.\n");
                }
            }
        }

        private static void EditFile(string textFileName)
        {
            var file = File.ReadLines(textFileName);
            while (true)
            {
                var commands = Editor.GetAllCommands();

                Console.WriteLine("\nPrint one of this command and press Enter:\n");
                foreach (var command in commands)
                {
                    Console.WriteLine($"{command.Key} - {command.Value}");
                }

                Console.WriteLine();

                var enteredCommand = Console.ReadLine();
                if (enteredCommand == null)
                {
                    Console.WriteLine("\nYou have to enter command\n");
                    return;
                }
                if (enteredCommand.Contains("del"))
                {
                    var lineNumber = int.Parse(enteredCommand.Replace("del", "").Trim());
                    Editor.DeleteLine(lineNumber, ref file);
                    var text = string.Join("\n", file.ToArray());
                    MessageBox.Show(text);
                }

                else if (enteredCommand.Contains("ins"))
                {
                    var lineNumber = int.Parse(enteredCommand.Replace("ins", "").Trim());
                    Editor.InsertLine(lineNumber, ref file);
                    var text = string.Join("\n", file.ToArray());
                    MessageBox.Show(text);
                }

                else
                {
                    switch (enteredCommand)
                    {
                        case "list":
                            var textFile = Editor.FormatFile(file);
                            MessageBox.Show(textFile);
                            break;
                        case "1":
                            Console.WriteLine(Editor.ShowLine(1, file));
                            break;
                        case "2":
                            Console.WriteLine(Editor.ShowLine(2, file));
                            break;
                        case "3":
                            Console.WriteLine(Editor.ShowLine(3, file));
                            break;
                        case "quit":
                            Environment.Exit(-1);
                            break;
                        case "save":
                            var text = string.Join("\n", file.ToArray());
                            Editor.Save(text);
                            break;
                        default:
                            Console.WriteLine("\nYou've entered unknown command. Please, try again.\n");
                            break;
                    }
                }
            }
        }
    }
}
