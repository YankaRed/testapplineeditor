﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace LineEditor
{
    public class Editor
    {
        public static Dictionary<string, string> GetAllCommands()
        {
            var commands = new Dictionary<string, string>();
            commands.Add("list", "returns list each line in n:xxx format");
            commands.Add("1", "returns first line");
            commands.Add("2", "returns second line");
            commands.Add("3", "returns last line");
            commands.Add("del n", "delete line at n");
            commands.Add("ins n", "insert a line at n");
            commands.Add("save", "saves to disk");
            commands.Add("quit", "quits the editor and returns to the command line");
            return commands;
        }

        public static string ShowLine(int number, IEnumerable<string> file)
        {
            switch (number)
            {
                case 1:
                    return file.First();
                case 2:
                    return file.ElementAt(1);
                case 3:
                    return file.Last();
                default:
                    return "";
            }
        }

        public static void DeleteLine(int number, ref IEnumerable<string> file)
        {
            var line = file.ElementAt(number - 1);
            var list  = file.Where(x => x != line).ToList();
            file = list;
        }

        public static void InsertLine(int number, ref IEnumerable<string> file)
        {
            var list = file.ToList();
            list.Insert(number - 1, "");
            file = list;
        }

        public static void Save(string text)
        {
            using (var sFile = new SaveFileDialog())
            {
                sFile.InitialDirectory = @"C:\";
                sFile.Title = "Save text Files";
                sFile.DefaultExt = "txt";
                sFile.Filter = "Text files (*.txt)|*.txt";
                sFile.FilterIndex = 1;
                sFile.RestoreDirectory = true;
                if (sFile.ShowDialog() == DialogResult.OK)
                {
                    var path = sFile.FileName;
                    if (!File.Exists(path))
                    {
                        var stream = File.Create(path);
                        stream.Close();
                    }
                    File.WriteAllText(path, text);
                    Console.WriteLine($"\nFile was succesfully saved to {Path.GetFullPath(path)}\n");
                }
            }
        }

        public static string FormatFile(IEnumerable<string> file)
        {
            var list = file.ToList();
            for (int i = 0; i < file.Count(); i++)
            {
                list[i] = $"{i+1}: {file.ElementAt(i)}";
            }

            return string.Join("\n", list.ToArray()); ;
        }
    }

}
